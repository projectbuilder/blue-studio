package com.github.cimela.e.restaurant.file.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.cimela.e.restaurant.base.appserver.BaseResponse;
import com.github.cimela.e.restaurant.base.service.AbstractComponentService;
import com.github.cimela.e.restaurant.file.appserver.FileRequest;
import com.github.cimela.e.restaurant.file.config.FileDataConfig;
import com.github.cimela.e.restaurant.file.model.FileDataVO;

@Service
public class FileDataServiceImpl extends AbstractComponentService<FileRequest, BaseResponse> implements FileDataService {

    @Autowired
    private FileDataConfig config;
    
    @Override
    public String getTarget() {
        return TARGET_FILE;
    }

    @Override
    public BaseResponse handle(FileRequest request) {
        BaseResponse response = new BaseResponse();
        switch (request.getType()) {
        case GET_ALL:
            response.setSuccess(true);
            response.setData(new FileDataVO());
            break;
        case UPDATE:
        default:
            break;

        }
        return response;
    }

    
}
