package com.github.cimela.e.restaurant.file.appserver;

import com.github.cimela.e.restaurant.base.appserver.BaseRequest;
import com.github.cimela.e.restaurant.file.model.FileDataVO;
import com.github.cimela.e.restaurant.file.model.FileTarget;

public class FileRequest extends BaseRequest<String> {
    
    private FileDataVO fileData;
    private FileTarget fileTarget;

    public FileDataVO getFileData() {
        return fileData;
    }

    public void setFileData(FileDataVO fileData) {
        this.fileData = fileData;
    }

	public FileTarget getFileTarget() {
		return fileTarget;
	}

	public void setFileTarget(FileTarget fileTarget) {
		this.fileTarget = fileTarget;
	}

}
