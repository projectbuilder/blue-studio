package com.github.cimela.e.restaurant.file.model;

import com.github.cimela.e.restaurant.base.model.GenericModelVO;

public class FileDataVO extends GenericModelVO<FileData> {

    public FileDataVO() {
        super(FileData.class);
    }
    
    public FileDataVO(FileData fileData, String...excludeAttrs) {
        super(fileData, FileData.class, excludeAttrs);
    }
    
}
