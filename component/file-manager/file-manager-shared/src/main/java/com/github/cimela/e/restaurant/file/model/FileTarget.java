package com.github.cimela.e.restaurant.file.model;

public enum FileTarget {
	SCENE, AVATAR;
}
