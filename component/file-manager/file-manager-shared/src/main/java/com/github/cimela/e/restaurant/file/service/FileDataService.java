package com.github.cimela.e.restaurant.file.service;

import com.github.cimela.e.restaurant.base.appserver.BaseResponse;
import com.github.cimela.e.restaurant.base.service.ComponentService;
import com.github.cimela.e.restaurant.file.appserver.FileRequest;

public interface FileDataService extends ComponentService<FileRequest, BaseResponse> {
    static final String TARGET_FILE = "file";
}
