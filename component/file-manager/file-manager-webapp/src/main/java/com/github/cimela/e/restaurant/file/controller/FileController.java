package com.github.cimela.e.restaurant.file.controller;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.cimela.e.restaurant.appserver.controller.BaseController;
import com.github.cimela.e.restaurant.appserver.util.RequestBuilder;
import com.github.cimela.e.restaurant.base.appserver.RequestType;
import com.github.cimela.e.restaurant.file.appserver.FileRequest;
import com.github.cimela.e.restaurant.file.model.FileDataVO;
import com.github.cimela.e.restaurant.file.model.FileTarget;
import com.github.cimela.e.restaurant.file.service.FileDataService;

@RestController
@RequestMapping(BaseController.PREFIX_API)
public class FileController extends BaseController {

    @GetMapping(path=FileDataService.TARGET_FILE + "/{name}", produces= MediaType.APPLICATION_JSON_VALUE)
    @CrossOrigin
    public FileDataVO findSample(HttpServletRequest request, @PathVariable String type) throws InstantiationException, IllegalAccessException {
        FileRequest sampleReq = RequestBuilder.request(FileRequest.class)
                                                .requestType(RequestType.GET_ALL)
                                                .requestUri(request.getServletPath())
                                                .build();
        sampleReq.setFileTarget(FileTarget.valueOf(type.toUpperCase(Locale.ENGLISH)));
        return serviceManager.handle(sampleReq);
    }
    
}
