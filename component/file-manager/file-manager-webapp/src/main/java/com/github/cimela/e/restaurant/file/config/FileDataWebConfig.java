package com.github.cimela.e.restaurant.file.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class FileDataWebConfig implements WebMvcConfigurer {
    
	private FileDataConfig fileConfig;
    
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/img/**").addResourceLocations("file:/" + fileConfig.getScene());
    }
    
}
