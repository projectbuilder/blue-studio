'use strict';

function AuthenticationController($scope, $log, $http, $window, $state, $filter, toastr, cfpLoadingBar, commonService, TokenStorage) {
    $scope.user = {};
    $scope.user.userEmail  = $window.localStorage.getItem('SESSION_USER_EMAIL');
    $scope.user.userPasswd = $window.localStorage.getItem('SESSION_USER_PASSWD');

    $scope.ozMobileState = this.ozMobileState;
    $scope.webState      = this.webState;

    $scope.loginLogo     = IMG_LOGIN_LOGO;

    this.login = function () {
        $log.debug("ozsds :: loginService - login");

        cfpLoadingBar.start();	// Loading Bar Start

        var req = {
            method: 'POST',
            url: './api/public/authen/v1/login',
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json; charset=utf-8'
            },
            data: JSON.stringify($scope.user)
        };

        var passwd = $scope.user.userPasswd;

        $http(req).success(function(response, status, headers, config) {
            $log.debug($filter('json')(response));

            $window.localStorage.setItem('SESSION_USER_NO',         response.userNo);
            $window.localStorage.setItem('SESSION_USER_ROLE',       response.userRoleCode);
            $window.localStorage.setItem('SESSION_USER_COMPANY',    response.userCompanyName);
            $window.localStorage.setItem('SESSION_USER_COMPANY_NO', response.userCompanyNo);
            $window.localStorage.setItem('SESSION_USER_IMG_EXIST',  response.companyImgExist);
            $window.localStorage.setItem('SESSION_USER_EMAIL',      response.userEmail);
            $window.localStorage.setItem('SESSION_USER_PASSWD',     passwd);

            TokenStorage.store(headers()['x-auth-token']);
            $state.go($scope.webState);

        }).error(function(data, status, headers, config) {
            toastr.error(data.data, "Authentication failed", TOASTR_CONFIG);
        }).finally(function() {
            cfpLoadingBar.complete();	// Loading Bar End
        });
    };
    $scope.logout = function() {
        $log.debug("loginService - logout");

        TokenStorage.clear();
        $state.go('login');
    };
}

// Register `phoneList` component, along with its associated controller and template
angular.module('authentication')
    .controller("authController", ['$scope', '$log', '$http', '$window', '$state', '$filter', 'toastr', 'cfpLoadingBar', 'commonService', 'TokenStorage', AuthenticationController])
    .component('login', {
        templateUrl: 'custom_components/authentication/authentication.login.template.html',
        controller: "authController",
        bindings: {
            ozMobileState: '<',
            webState: '<'
        }
    });
