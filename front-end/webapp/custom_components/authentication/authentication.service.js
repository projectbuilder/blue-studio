'use strict';

function AuthenticationService($window, $log, $http) {
    return {
        applySession: function($scope) {
            $scope.userNo          = $window.localStorage.getItem('SESSION_USER_NO');
            $scope.userRoleCode    = $window.localStorage.getItem('SESSION_USER_ROLE');
            $scope.userCompanyName = $window.localStorage.getItem('SESSION_USER_COMPANY');
            $scope.userCompanyNo   = $window.localStorage.getItem('SESSION_USER_COMPANY_NO');
            $scope.userEmail       = $window.localStorage.getItem('SESSION_USER_EMAIL');
            $scope.companyImgExist = $window.localStorage.getItem('SESSION_USER_IMG_EXIST');
        },
        isLogin: function() {
            $log.debug("authService - isLogin");

            var req = {
                method: 'GET',
                url: './api/v1/public/is_login',
                dataType: 'json',
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            };
            return $http(req);
        }
    };
}
AuthenticationService.$inject = ['$window', '$log', '$http'];

// Register `phoneList` component, along with its associated controller and template
angular.module('authentication')
    .service('authService', AuthenticationService);