function authConfig($stateProvider, $urlRouterProvider, $httpProvider) {
    $httpProvider.interceptors.push('accessHeaderInterceptor');
    $urlRouterProvider.otherwise("/login");

    $stateProvider
        .state('login', {
            url: "/login",
            component: 'login',
            resolve: {
                ozMobileState: function() {
                    return 'mobile.products';
                },
                webState: function() {
                    return 'web.complete.list';
                }
            }
        })
        .state('raon', {
            url: "/raon?completeNo",
            controller: ['$state', '$stateParams', '$window', 'commonService', function($state, $stateParams, $window, commonService) {
                if(commonService.PLATFORM.isMobile()) {
                    $window.open(`raon://?${document.location.origin}${document.location.pathname}${$state.href('ozConfirm', {completeNo: $stateParams.completeNo })}`);
                } else {
                    $state.go('ozConfirm', {completeNo: $stateParams.completeNo });
                }
            }]
        })
        .state('raonEdit', {
            url: "/raon/edit?completeNo",
            controller: ['$state', '$stateParams', '$window', 'commonService', function($state, $stateParams, $window, commonService) {
                if(commonService.PLATFORM.isMobile()) {
                    $window.open(`raon://?${document.location.origin}${document.location.pathname}${$state.href('web.forms.ozWeb', {completeNo: $stateParams.completeNo })}`);
                } else {
                    $state.go('web.forms.ozWeb', {completeNo: $stateParams.completeNo });
                }
            }]
        })
        .state('raonRegister', {
            url: "/raon/register",
            controller: ['$window', function($window) {
                $window.open(`raon://?test`);
            }]
        });
}

angular.module('authentication')
       .config(authConfig);
