function accessHeaderInterceptor($window, $q, $log, $state, TokenStorage) {
    var timeout = 3000;
    return {
        request: function(config) {
            $log.debug("Add authorization token");
            config.headers = config.headers || {};
            var token = TokenStorage.retrieve();
            if (token) {
                config.headers['X-Auth-Token'] = token;

                return config;
            }
            return config;
        },
        response: function(response) {
            switch (response.status) {
                case 401:
                    TokenStorage.clear();
                    $state.go('login');
                    break;
                case 403:
                    // toastr.error("You do not have authority to execute this action", "Error", {
                    //     showCloseButton: true,
                    //     timeout: timeout
                    // });
                    return $q.reject(response);
            }
            return response;
        }
    };
}

function tokenStorage() {
    var storageKey = "auth_token";
    return {
        store: function(token) {
            return localStorage.setItem(storageKey, token);
        },
        retrieve: function() {
            return localStorage.getItem(storageKey);
        },
        clear: function() {
            return localStorage.removeItem(storageKey);
        }
    };
}

angular.module('authentication')
    .factory('accessHeaderInterceptor', accessHeaderInterceptor)
    .factory('TokenStorage', tokenStorage);

