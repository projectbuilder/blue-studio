angular.module('authentication')
       .run(function($rootScope, $state, $log, $window, $filter, $transitions, TokenStorage) {
            var criteria = {
                to: function(state) {
                    return state.authenticate;
                }
            };

            $transitions.onBefore(criteria, function(transition) {
                if(!TokenStorage.retrieve()) {
                    return transition.router.stateService.target('login');
                }
            });

       });