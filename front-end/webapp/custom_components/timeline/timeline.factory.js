'use strict';

function timelineFactory($log, $http, cfpLoadingBar) {
    return {
        timeline: function() {
            $log.debug("timelineFactory :: timeline");
            cfpLoadingBar.start();
            var promise = $http.get('data/timeline.json');
            promise.then(function (response, status, headers, config) {
                // success case
                cfpLoadingBar.complete();	// Loading Bar End
                return response.data;
            }, function (response, status, headers, config) {
                $log.error("Cannot get timeline");
                cfpLoadingBar.complete();	// Loading Bar End
                return null;
            });

            return promise;
        }
    }
}

angular.module('timeline-layer')
    .factory('timelineFactory', ["$log", "$http", "cfpLoadingBar", timelineFactory]);
