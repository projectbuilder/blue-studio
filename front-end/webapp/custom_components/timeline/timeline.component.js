angular.module('timeline-layer')
    .component('timelineLayer', {
        templateUrl: 'custom_components/timeline/timeline.template.html',
        controller: function() {},
        bindings: {
            header:   '<',
            description:   '<',
            timeline:  '<'
        }
    });
