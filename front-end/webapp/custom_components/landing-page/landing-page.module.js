'use strict';

// Define the `landing` module
angular.module('landing-page', [
    'uiCore',
    'i18n',
    'feature-layer',
    'team-layer',
    'timeline-layer',
    'client-comment-layer',
    'addition-features-layer',
    'pricing-layer',
    'contact-layer'
]);
