function SideBarCtrl($rootScope, $scope, $log, $translate, $mdSidenav) {

    $scope.theme = $rootScope.theme;
    $scope.language = $translate.use();
    var ctrl = this;
    ctrl.$onInit = function() {
        $scope.details = [];
        for(var i = 1; i <= ctrl.details; i++) {
            $scope.details.push({
                title: ctrl.titlePrefix + i,
                content: ctrl.contentPrefix + i
            });
        }
    };

    $scope.changeLanguage = function(languageKey) {
        $log.debug("Change language to " + languageKey);
        $translate.use(languageKey);
        $scope.language     = languageKey;
    };

    $scope.closeSetting = function(id) {
        $mdSidenav(id).close();
    };

}

angular.module('landing-page')
    .component('sideBar', {
        templateUrl: 'custom_components/landing-page/landing-page.side-bar.template.html',
        controller: ['$rootScope', '$scope', '$log', '$translate', '$mdSidenav', SideBarCtrl],
        bindings: {
            sideTitle: '<',
            sideBarId: '<?',
            languages: '<',
            details: '<',
            titlePrefix: '<?',
            contentPrefix: '<?',
        }
    });
