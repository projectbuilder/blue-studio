function LandingPageCtrl($scope, $log, $state, $uibModal, toastr) {

    $log.debug("product list ");

    var ctrl = this;

    $scope.reports = [];
    $scope.cmail = "";
    // $scope.productImg = IMG_PRODUCT;

    $scope.$state = $state;
    $scope.$location = location;

    $scope.photos = [
        {
            path: "img/avatar1.jpg",
            header: "Some description"
        },
        {
            path: "img/avatar2.jpg",
            header: "Some description2"
        }
    ];

    $scope.album = ["img/avatar1.jpg", "img/avatar2.jpg"];

    $scope.showPhoto = function(title, photo) {
        $log.debug("open photo " + photo);
        $uibModal.open({
            component: 'photoBooth',
            size: 'lg',		// sm, md, lg
            resolve: {
                album: function() {
                    return photo;
                },
                title: function() {
                    return title;
                }
            }
        }).result.then(function() {
            $log.debug("Close modal");
        }, function() {
            $log.debug("Close modal");
        });
    };

    $scope.showAlbum = function(title, photo, prefix) {
        $log.debug("open album " + photo);
        $uibModal.open({
            component: 'photoAlbum',
            size: 'lg',		// sm, md, lg
            resolve: {
                album: function() {
                    return photo;
                },
                title: function() {
                    return title;
                },
                titlePrefix: function() {
                    return prefix;
                }
            }
        }).result.then(function() {
            $log.debug("Close modal");
        }, function() {
            $log.debug("Close modal");
        });
    };

    ctrl.$onInit = function() {
        $log.debug(ctrl.navItems);
    };

}

angular.module('landing-page')
    .component('landingPage', {
        templateUrl: 'custom_components/landing-page/landing-page.resume.html',
        controller: ['$scope', '$log', '$state', '$uibModal', 'toastr', LandingPageCtrl],
        bindings: {
            navItems: '<',
            languages: '<',
            bannerItems: '<',
            video: '<',
            serviceList: '<',
            feature: '<',
            sideFeature: '<',
            certificates: '<',
            team: '<',
            timeline: '<',
            users: '<',
            partners: '<',
            addition: '<',
            pricing: '<',
            contact: '<'
        }
});
