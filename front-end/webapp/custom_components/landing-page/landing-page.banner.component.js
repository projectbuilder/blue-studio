angular.module('landing-page')
    .component('photoBanner', {
        templateUrl: 'custom_components/landing-page/landing-page.banner.template.html',
        controller: ['$scope','$log', function($scope, $log) {
            $log.debug(this);
            $scope.interval = 3000;
        }],
        bindings: {
            items: '<'
        }
    });
