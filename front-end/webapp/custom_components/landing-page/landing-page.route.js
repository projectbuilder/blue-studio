function landingPageConfig($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise("/");
    $stateProvider
        .state('landing', {
            url: "/",
            component: 'landingPage',
            resolve: {
                navItems: function() {
                    return {
                        'NAV_HOME': '#page-top',
                        'FEATURE_HEADER': '#projects',
                        'TIMELINE_HEADER': '#timeline',
                        'NAV_EXP': '#experience',
                        'NAV_CONTACT': '#contact'
                    }
                },
                languages: function() {
                    return ['vn', 'us'];
                },
                bannerItems: ['landingPageFactory', function(landingPageFactory) {
                    return landingPageFactory.bannerInfo();
                }],
                video: ['featureFactory', function(featureFactory) {
                    return featureFactory.video();
                }],
                serviceList: ['featureFactory', function(featureFactory) {
                    return featureFactory.services();
                    // return [
                    //     {
                    //         name: 'FULL_RESPONSIVE',
                    //         description: 'FULL_RESPONSIVE_DESC',
                    //         // path: 'sample'
                    //     },
                    //     {
                    //         name: 'LESS_SASS_FILES',
                    //         description: 'LESS_SASS_FILES_DESC',
                    //         // path: 'sample'
                    //     },
                    //     {
                    //         name: 'CHARTS_LIBRARY',
                    //         description: 'CHARTS_LIBRARY_DESC',
                    //         // path: 'sample'
                    //     },
                    //     {
                    //         name: 'ADVANCED_FORMS',
                    //         description: 'ADVANCED_FORMS_DESC',
                    //         path: 'sample'
                    //     }
                    // ]
                }],
                feature: ['featureFactory', function(featureFactory) {
                    return featureFactory.features();
                }],
                sideFeature: ['featureFactory', function(featureFactory) {
                    return featureFactory.sideFeature();
                }],
                team: ['teamFactory', function(teamFactory) {
                    return teamFactory.members();
                }],
                certificates: ['featureFactory', function(featureFactory) {
                    return featureFactory.certificates();
                }],
                timeline: ["timelineFactory", function(timelineFactory) {
                    return timelineFactory.timeline();
                }],
                users: function() {
                    return {
                        header: 'USER_COMMENT',
                        content: 'USER_COMMENT_CONTENT',
                        date: 132546,
                        name: 'Andy Smith'
                    }
                },
                partners: ["commentFactory", function(commentFactory) {
                    return commentFactory.partnerComments();
                }],
                addition: ["additionFactory", function(additionFactory) {
                    return additionFactory.additions();
                }],
                pricing: ['priceFactory', function(priceFactory) {
                    return priceFactory.prices();
                }],
                contact: ['contactFactory', function(contactFactory) {
                    return contactFactory.contactInfo();
                }]
            }
        })
}
landingPageConfig.$inject = ['$stateProvider', '$urlRouterProvider'];

angular.module('landing-page')
    .config(landingPageConfig)
    .run(['$rootScope', function($rootScope){
        $rootScope.theme = "teal";
    }]);