function NavigationCtrl($scope, $log, $translate, $mdSidenav) {
    $scope.theme = 'teal';
    $scope.language = $translate.use();
    var ctrl = this;
    ctrl.$onInit = function() {
        // $scope.languageText = ctrl.languages[$scope.language];
    };

    $scope.changeLanguage = function(languageKey, languageText) {
        $log.debug("Change language to " + languageKey);
        $translate.use(languageKey);
        $scope.language = languageKey;
        $scope.languageText = languageText;
    };

    $scope.openSettings = function(navID) {
        // Component lookup should always be available since we are not using `ng-if`
        $mdSidenav(navID).toggle();
    };

    $scope.closeSetting = function() {
        $mdSidenav('right').close();
    };

}

angular.module('landing-page')
    .component('navigationBar', {
        templateUrl: 'custom_components/landing-page/landing-page.navigation-bar.template.html',
        controller: ['$scope', '$log', '$translate', '$mdSidenav', NavigationCtrl],
        bindings: {
            navItems: '<',
            sideBarId: '<?'
        }
    });
