function landingPageFactory($log, $http, cfpLoadingBar) {
    return {
        bannerInfo: function () {
            $log.debug("landingPageFactory :: bannerInfo");
            cfpLoadingBar.start();
            var promise = $http.get('data/header.json');
            promise.then(function (response, status, headers, config) {
                // success case
                cfpLoadingBar.complete();	// Loading Bar End
                return response.data;
            }, function (response, status, headers, config) {
                $log.error("Cannot get banner data info");
                cfpLoadingBar.complete();	// Loading Bar End
                return null;
            });

            return promise;
        }
    }
}

angular.module('landing-page')
    .factory('landingPageFactory', ['$log', '$http', 'cfpLoadingBar', landingPageFactory]);