'use strict';

// Define the `i18n` module
angular.module('i18n', [
    'pascalprecht.translate'
]);