function i18nConfig($translateProvider, $translatePartialLoaderProvider) {
    $translateProvider.useLoaderCache(true);
    $translateProvider.useLoader('$translatePartialLoader', {
        urlTemplate: '/custom_components/{part}/lang/{lang}.json',
        loadFailureHandler: 'TranslateErrorHandler'
    });
    $translateProvider.preferredLanguage('us');

    $translatePartialLoaderProvider.addPart('ui-core');
    $translatePartialLoaderProvider.addPart('landing-page');
    $translatePartialLoaderProvider.addPart('timeline');
    $translatePartialLoaderProvider.addPart('team');
    $translatePartialLoaderProvider.addPart('features');
    $translatePartialLoaderProvider.addPart('client-comment');
    $translatePartialLoaderProvider.addPart('pricing');
    $translatePartialLoaderProvider.addPart('contact');
    $translatePartialLoaderProvider.addPart('addition-features');
}
i18nConfig.$inject = ['$translateProvider', '$translatePartialLoaderProvider'];

angular.module('i18n')
    .config(i18nConfig);