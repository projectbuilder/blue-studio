function TranslateErrorHandler($q, $log) {
    return function (part, lang, response) {
        $log.error('The "' + part + '/' + lang + '" part was not loaded.');
        return $q.when({});
    };
}
TranslateErrorHandler.$inject = ['$q', '$log'];

angular.module('i18n')
    .factory('TranslateErrorHandler', TranslateErrorHandler);