angular.module('uiCore')
    .run(['$rootScope', '$state', '$log', '$window', '$transitions', function($rootScope, $state, $log, $window, $transitions) {
        $log.debug("Verify the size of current window");
        $rootScope.smallDevice = $window.innerWidth < 700;
    }]);