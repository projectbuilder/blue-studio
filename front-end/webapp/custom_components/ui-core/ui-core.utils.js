function Utils($q) {
    return {
        isImage: function(src) {
            var deferred = $q.defer();

            var image = new Image();
            image.onerror = function() {
                deferred.resolve(false);
            };
            image.onload = function() {
                deferred.resolve(true);
            };
            image.src = src;

            return deferred.promise;
        }
    };
}

angular.module('uiCore')
       .factory('Utils', ['$q', Utils]);