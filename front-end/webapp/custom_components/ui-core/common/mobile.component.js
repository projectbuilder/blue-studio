function MobileViewController($scope, $log, authService, Utils) {
    authService.applySession($scope);
    var srcImg = 'upload/img/' + $scope.userCompanyName + '/logo.png';
    Utils.isImage(srcImg).then(function(isExisted) {
        if(isExisted) {
            $log.info("Apply company Logo image for " + $scope.userCompanyName);
            $scope.logoImg = srcImg;
        }
    });
}

angular.module('uiCore')
       .component('mobileView',{
           templateUrl: 'custom_components/ui-core/common/mobile.content.html',
           controller: [ '$scope', '$log', 'authService', 'Utils', MobileViewController]
       });