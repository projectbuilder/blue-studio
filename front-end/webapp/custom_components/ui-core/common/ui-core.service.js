function commonService($log, $http, $translate, cfpLoadingBar, toastr) {
    var methods = {
        PLATFORM: {
            isAndroid: function () {
                return navigator.userAgent.match('Android');
            },
            isIPhone: function () {
                return navigator.userAgent.match('iPhone|iPod');
            },
            isIPad: function () {
                return navigator.userAgent.match('iPad');
            },
            isWebView: function () {
                return navigator.userAgent.match('WebView');
            },
            isWindows: function () {
                return navigator.userAgent.match('Windows');
            },
            isMobile: function () {
                return navigator.userAgent.match('iPhone|iPod|iPad|Android');
            },
            isTotoView: function () {
                return navigator.userAgent.match("OZTotoFramework");
            }
        },
        TOASTER: {
            error: function(message, title) {
                methods.TOASTER.display('error', message, title);
            },
            info: function(message, title) {
                methods.TOASTER.display('info', message, title);
            },
            warn: function(message, title) {
                methods.TOASTER.display('warn', message, title);
            },
            display: function(type, message, title) {
                var translate = typeof message === 'object' ? $translate([message.key, title], message.params) : $translate([message, title]);
                translate.then(function(translated) {
                    toastr.pop({
                        type: type,
                        title: translated[title],
                        body: typeof message === 'object' ? translated[message.key] : translated[message],
                        showCloseButton: true,
                        timeout: 3000
                    });
                }, function(translated){
                    toastr.pop({
                        type: type,
                        title: translated[title],
                        body: typeof message === 'object' ? translated[message.key] : translated[message],
                        showCloseButton: true,
                        timeout: 3000
                    });
                });
            }
        },
        getResources: function(filePath) {
            cfpLoadingBar.start();
            var promise = $http.get(filePath);
            promise.then(function (response, status, headers, config) {
                // success case
                cfpLoadingBar.complete();	// Loading Bar End
                return response.data;
            }, function (response, status, headers, config) {
                $log.error("Cannot get" + filePath);
                cfpLoadingBar.complete();	// Loading Bar End
                return null;
            });

            return promise;
        }
    };

    return methods;
}

angular.module('uiCore')
    .service('commonService', ['$log', '$http', '$translate', 'cfpLoadingBar', 'toastr', commonService]);
