function PopUpPhotoAlbumCtrl($scope, $log) {

    $log.debug("Open photo album");

    var $ctrl = this;
    $ctrl.$onInit = function () {
        var count = 1;
        $scope.album = [];
        $ctrl.resolve.album.forEach(function(photo) {
            $scope.album.push({
                path: photo,
                title: $ctrl.resolve.titlePrefix ? $ctrl.resolve.titlePrefix + count++ : undefined
            })
        });
        $scope.title = $ctrl.resolve.title;
    };

    $scope.interval = 3000;
}

angular
    .module('uiCore')
    .component('photoAlbum', {
        templateUrl: 'custom_components/ui-core/pop-up/ui-core.photo-album.html',
        controller: ['$scope', '$log', PopUpPhotoAlbumCtrl],
        bindings: {
            resolve: '<',
            close: '&',
            dismiss: '&'
        }
    });