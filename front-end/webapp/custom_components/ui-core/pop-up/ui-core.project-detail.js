function ProjectDetailsCtrl($scope, $log) {

    $log.debug("Open project details");
    $scope.quantity = $scope.maxQuantity = 4;

    $scope.changeQuantity = function(quantity) {
        $scope.quantity = quantity;
    };

    var $ctrl = this;
    $ctrl.$onInit = function () {
        $scope.title    = $ctrl.resolve.title;
        $scope.photo    = $ctrl.resolve.photo;

        $scope.details  = [];
        for(var i = 1; i <= $ctrl.resolve.details; i++) {
            $scope.details.push({
                title: ($ctrl.resolve.titlePattern ? $ctrl.resolve.titlePattern : 'FEATURE_TITLE_') + i,
                content: ($ctrl.resolve.contentPattern ? $ctrl.resolve.contentPattern : 'FEATURE_' + $ctrl.resolve.no + '_CONTENT_')  + i
            });
        }
    };
}

angular
    .module('uiCore')
    .component('projectDetails', {
        templateUrl: 'custom_components/ui-core/pop-up/ui-core.project-detail.template.html',
        controller: ['$scope', '$log', ProjectDetailsCtrl],
        bindings: {
            resolve: '<',
            close: '&',
            dismiss: '&'
        }
    });