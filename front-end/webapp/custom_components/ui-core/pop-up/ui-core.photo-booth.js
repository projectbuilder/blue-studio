function PopUpPhotoBoothCtrl($scope, $log) {

    $log.debug("Open photo booth");

    var $ctrl = this;
    $ctrl.$onInit = function () {
        $scope.album = $ctrl.resolve.album;
        $scope.title = $ctrl.resolve.title;
    };

    $scope.interval = 3000;
}

angular
    .module('uiCore')
    .component('photoBooth', {
        templateUrl: 'custom_components/ui-core/pop-up/ui-core.photo-booth.template.html',
        controller: ['$scope', '$log', PopUpPhotoBoothCtrl],
        bindings: {
            resolve: '<',
            close: '&',
            dismiss: '&'
        }
    });