function TabsCtrl($state) {
    var vm = this;
    vm.tabSelected = function (route) {
        $state.go(route);
    };
}

/**
 *
 * Pass all functions into module
 */
angular
    .module('uiCore')
    .component('leftTabs', {
        templateUrl: 'custom_components/ui-core/common/left-tabs.html',
        controller: ['$state', TabsCtrl],
        bindings: {
            appStates: '<'
        }
    });
