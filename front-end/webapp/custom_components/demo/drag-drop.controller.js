function DragDropController($scope) {
    $scope.hiddenList  = [];
    $scope.displayList = [];

    $scope.sortOptions = {

        //restrict move across columns. move only within column.
        /*accept: function (sourceItemHandleScope, destSortableScope) {
         return sourceItemHandleScope.itemScope.sortableScope.$id === destSortableScope.$id;
         },*/
        itemMoved: function (event) {
            // event.source.itemScope.modelValue.status = event.dest.sortableScope.$parent.column.name;
            event.source.itemScope.modelValue.visible = !event.source.itemScope.modelValue.visible;
            sortList($scope.hiddenList);
            sortList($scope.displayList);
        },
        orderChanged: function (event) {
            var list  = event.source.itemScope.modelValue.visible ? $scope.displayList : $scope.hiddenList;
            sortList(list);
        },
        containment: '#board'
    };

    $scope.switchAll = function(visible) {
        var source, target;

        if(visible) {
            source = $scope.hiddenList;
            target = $scope.displayList;

            $scope.hiddenList = [];
        } else {
            source = $scope.displayList;
            target = $scope.hiddenList;

            $scope.displayList = [];
        }

        var item;
        for(var i=0; i < source.length; i++) {
            item = source[i];
            item.order = target.length;
            item.visible = visible;

            target.push(item);
        }
    };

    var sortList = function(list) {
        for(var i = 0; i < list.length; i++) {
            list[i].order = i + 1;
        }
    };
}