angular.module('team-layer')
    .component('teamLayer', {
        templateUrl: 'custom_components/team/team.template.html',
        controller: function() {},
        bindings: {
            header:   '<',
            description: '<',
            summary:  '<',
            members:  '<'
        }
    });
