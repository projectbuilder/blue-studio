'use strict';

function teamFactory($log, $http, cfpLoadingBar, commonService) {
    return {
        members: function() {
            $log.debug("teamFactory :: members");
            return commonService.getResources('data/members.json');
        }
    }
}

angular.module('team-layer')
    .factory('teamFactory', ["$log", "$http", "cfpLoadingBar", "commonService", teamFactory]);
