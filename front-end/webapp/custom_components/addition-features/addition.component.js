function AdditionFeatures($scope, $log, $uibModal) {

    $scope.showDetail = function(item){
        $log.debug("Open details of " + item.title);
        $uibModal.open({
            component: 'projectDetails',
            size: 'lg',		// sm, md, lg
            resolve: {
                title: function() {
                    return item.title;
                },
                photo: function() {
                    return item.photo;
                },
                details: function() {
                    return item.details;
                },
                titlePattern: function() {
                    return item.titlePattern;
                },
                contentPattern: function() {
                    return item.contentPattern;
                }
            }
        }).result.then(function() {
            $log.debug("Close modal");
        }, function() {
            $log.debug("Close modal");
        });
    }
}

angular.module('addition-features-layer')
    .component('additionFeaturesLayer', {
        templateUrl: 'custom_components/addition-features/addition.template.html',
        controller: ["$scope", "$log", "$uibModal", AdditionFeatures],
        bindings: {
            header:   '<',
            description:   '<',
            additionFeatures:  '<'
        }
    });
