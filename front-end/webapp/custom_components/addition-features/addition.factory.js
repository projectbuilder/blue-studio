'use strict';

function additionFactory($log, $http, cfpLoadingBar, commonService) {
    return {
        additions: function() {
            $log.debug("additionFactory :: additions");
            return commonService.getResources('data/addition.json');
        }
    }
}

// Define the `feature` module
angular.module('addition-features-layer')
    .factory('additionFactory', ['$log', '$http', 'cfpLoadingBar', 'commonService', additionFactory]);