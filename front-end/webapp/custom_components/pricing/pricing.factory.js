'use strict';

function priceFactory($log, $http, cfpLoadingBar, commonService) {
    return {
        prices: function() {
            $log.debug("priceFactory :: prices");
            return commonService.getResources('data/pricing.json');
        }
    }
}

angular.module('pricing-layer')
    .factory('priceFactory', ["$log", "$http", "cfpLoadingBar", "commonService", priceFactory]);