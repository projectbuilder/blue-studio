angular.module('pricing-layer')
    .component('pricingLayer', {
        templateUrl: 'custom_components/pricing/pricing.template.html',
        controller: function() {},
        bindings: {
            header:   '<',
            description:  '<',
            prices: '<'
        }
    });
