/**
 * INSPINIA - Responsive Admin Theme
 *
 * Inspinia theme use AngularUI Router to manage routing and views
 * Each view are defined as state.
 * Initial there are written state for all view in theme.
 *
 */
function config($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, $logProvider, $httpProvider, $compileProvider, cfpLoadingBarProvider, IdleProvider, KeepaliveProvider) {
    // function config($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, $logProvider, $httpProvider, $compileProvider, cfpLoadingBarProvider) {

    // Loading Bar
    cfpLoadingBarProvider.includeSpinner = true;

    // 로그 처리
    $logProvider.debugEnabled(true);

    // 인증 처리
    // $httpProvider.interceptors.push('accessHeaderInterceptor');

    // 에러처리
    // httpProvider.interceptors.push("ErrorInterceptor");

    // Configure Idle settings
    IdleProvider.idle(5); // in seconds
    IdleProvider.timeout(120); // in seconds

    $ocLazyLoadProvider.config({
        // Set to true if you want to see what and when is dynamically loaded
        debug: true
    });

    // URL State Provider
    // $stateProvider
    //
    //     .state('mobile', {
    //         abstract: true,
    //         url: "/mobile",
    //         component: "mobileView"
    //     })
    //     .state('web', {
    //         abstract: true,
    //         templateUrl: "custom_components/ui-core/common/web.content.html"
    //     })
}

angular
    .module('BlueStudio')
    .config(config);