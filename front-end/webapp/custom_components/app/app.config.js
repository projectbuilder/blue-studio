function appConfig($compileProvider, $sceDelegateProvider) {
    $sceDelegateProvider.resourceUrlWhitelist(['^(?:http(?:s)?:\/\/)?(?:[^\.]+\.)?\(vimeo|youtube)\.com(/.*)?$', 'self']);
}

appConfig.$inject = ['$compileProvider', '$sceDelegateProvider'];

angular.module('BlueStudio')
    .config(appConfig);