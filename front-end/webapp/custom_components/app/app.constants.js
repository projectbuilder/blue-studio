// common constants
const TOASTR_TIMEOUT = 3000;
const TOASTR_CONFIG = {
    timeout: TOASTR_TIMEOUT,
    closeButton: true
};
const DEFAULT_COUNTRY = 'sg'; // when using as internal system without internet connection
