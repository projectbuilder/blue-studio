/**
 * INSPINIA - Responsive Admin Theme
 *
 */
(function () {
	angular.module('BlueStudio', [
		'ui.router',					// Routing
		'oc.lazyLoad',					// ocLazyLoad
		'ui.bootstrap',					// Ui Bootstrap
		'ui.bootstrap.carousel',
		'ngIdle.idle',
		'pascalprecht.translate',		// Angular Translate
		'cfp.loadingBar',				// loadingBar
		'toastr',						// toaster
        'ngResource',
        'ngMaterial',

        'uiCore',
		'i18n',
		'landing-page'
	])
})();
