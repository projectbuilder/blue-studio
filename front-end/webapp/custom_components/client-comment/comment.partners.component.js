angular.module('client-comment-layer')
    .component('partnersCommentLayer', {
        templateUrl: 'custom_components/client-comment/comment.partners.template.html',
        controller: function() {},
        bindings: {
            header: '<',
            description: '<',
            comments: '<'
        }
    });
