'use strict';

function commentFactory($log, $http, cfpLoadingBar) {
    return {
        partnerComments: function() {
            $log.debug("commentFactory :: partnerComments");
            cfpLoadingBar.start();
            var promise = $http.get('data/partners.json');
            promise.then(function (response, status, headers, config) {
                // success case
                cfpLoadingBar.complete();	// Loading Bar End
                return response.data;
            }, function (response, status, headers, config) {
                $log.error("Cannot get comments");
                cfpLoadingBar.complete();	// Loading Bar End
                return null;
            });

            return promise;
        }
    }
}

// Define the `feature` module
angular.module('client-comment-layer')
    .factory('commentFactory', ['$log', '$http', 'cfpLoadingBar', commentFactory]);