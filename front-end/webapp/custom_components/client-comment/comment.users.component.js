angular.module('client-comment-layer')
    .component('usersCommentLayer', {
        templateUrl: 'custom_components/client-comment/comment.users.template.html',
        controller: function() {},
        bindings: {
            commentHeader: '<',
            content: '<',
            date: '<',
            username: '<'
        }
    });
