angular.module('contact-layer')
    .component('contactLayer', {
        templateUrl: 'custom_components/contact/contact.template.html',
        controller: function() {},
        bindings: {
            header:   '<',
            description:  '<',
            company: '<',
            summary: '<'
        }
    });
