'use strict';

function contactFactory($log, $http, cfpLoadingBar) {
    return {
        contactInfo: function() {
            $log.debug("contactFactory :: contactInfo");
            cfpLoadingBar.start();
            var promise = $http.get('data/contact.json');
            promise.then(function (response, status, headers, config) {
                // success case
                cfpLoadingBar.complete();	// Loading Bar End
                return response.data;
            }, function (response, status, headers, config) {
                $log.error("Cannot get services");
                cfpLoadingBar.complete();	// Loading Bar End
                return null;
            });

            return promise;
        },
        features: function() {
            $log.debug("featureFactory :: features");
            cfpLoadingBar.start();
            var promise = $http.get('data/features.json');
            promise.then(function (response, status, headers, config) {
                // success case
                cfpLoadingBar.complete();	// Loading Bar End
                return response.data;
            }, function (response, status, headers, config) {
                $log.error("Cannot get features");
                cfpLoadingBar.complete();	// Loading Bar End
                return null;
            });

            return promise;
        }
    }
}

// Define the `contact` module
angular.module('contact-layer')
    .factory('contactFactory', ['$log', '$http', 'cfpLoadingBar', contactFactory]);