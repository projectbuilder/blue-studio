angular.module('feature-layer')
    .component('serviceLayer', {
        templateUrl: 'custom_components/features/services.template.html',
        controller: function() {},
        bindings: {
            services: '<'
        }
    });
