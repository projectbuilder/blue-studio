'use strict';

function featureFactory($log, $http, cfpLoadingBar, commonService) {
    return {
        services: function() {
            $log.debug("featureFactory :: services");
            return commonService.getResources('data/services.json');
        },
        features: function() {
            $log.debug("featureFactory :: features");
            return commonService.getResources('data/features.json');
        },
        video: function() {
            $log.debug("featureFactory :: video");
            return commonService.getResources('data/video.json');
        },
        sideFeature: function() {
            $log.debug("featureFactory :: side");
            return commonService.getResources('data/side.json');
        },
        certificates: function() {
            $log.debug("featureFactory :: certificates");
            return commonService.getResources('data/certificates.json');
        }
    }
}

// Define the `feature` module
angular.module('feature-layer')
    .factory('featureFactory', ['$log', '$http', 'cfpLoadingBar', 'commonService', featureFactory]);