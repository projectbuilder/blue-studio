angular.module('feature-layer')
    .component('videoLayer', {
        templateUrl: 'custom_components/features/video.template.html',
        controller: ['$scope', '$log', '$sce', function($scope, $log, $sce) {
            var vm = this;
            $scope.videoList = [];
            vm.$onInit = function() {
              $log.debug(vm);
              $scope.videoList.push($sce.trustAsResourceUrl(vm.video + '?autoplay=1&fs=1&iv_load_policy=3&showinfo=0&rel=0&cc_load_policy=0&amp;showinfo=0'));
              // $scope.videoList.push(vm.video + '?autoplay=1&mute=1&enablejsapi=1');
            };
        }],
        bindings: {
            header:   '<',
            description:  '<',
            highlight:    '<',
            title: '<',
            content: '<',
            link: '<?',
            action: '<?',
            video: '<',
            reverse: '<?'
        }
    });
