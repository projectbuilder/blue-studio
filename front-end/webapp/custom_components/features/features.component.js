function FeatureLayerCtrl($scope, $log, $uibModal) {
    $log.debug("Feature layer");

    $scope.showDetail = function(number, feature){
        $log.debug("Open details of " + feature.title);
        $uibModal.open({
            component: 'projectDetails',
            size: 'lg',		// sm, md, lg
            resolve: {
                title: function() {
                    return feature.title;
                },
                photo: function() {
                    return feature.photo;
                },
                details: function() {
                    return feature.details;
                },
                no: function() {
                    return number;
                }
            }
        }).result.then(function() {
            $log.debug("Close modal");
        }, function() {
            $log.debug("Close modal");
        });
    }
}

angular.module('feature-layer')
    .component('featureLayer', {
        templateUrl: 'custom_components/features/features.template.html',
        controller: ['$scope', '$log', '$uibModal', FeatureLayerCtrl],
        bindings: {
            header: '<',
            description: '<',
            highlight: '<',
            leftFeatures:  '<',
            featureImg:    '<',
            rightFeatures: '<'
        }
    });
