angular.module('feature-layer')
    .component('sideFeatureLayer', {
        templateUrl: 'custom_components/features/side-feature.template.html',
        controller: function() {},
        bindings: {
            header: '<',
            description: '<',
            highlight: '<',
            title: '<',
            content: '<',
            photo: '<',
            actionText: '<?',
            action: '&?',
            reverse: '<?'
        }
    });
